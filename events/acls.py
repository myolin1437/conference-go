from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY

import json
import requests


def get_photo(city, state):
    headers = {
        "Authorization": PEXELS_API_KEY,
    }

    # url = f"https://api.pexels.com/v1/search?query={city}-{state}&per_page=1"
    # r = requests.get(url, headers=headers)

    params = {"per_page": 1, "query": f"{city}-{state}"}
    url = "https://api.pexels.com/v1/search"
    r = requests.get(url, params=params, headers=headers)

    api_response_content = json.loads(r.content)

    try:
        return {"picture_url": api_response_content["photos"][0]["url"]}
    except (KeyError, IndexError):
        return {"picture_url": None}


def get_weather(city, state):
    # Translate the city and state for the conference into latitude and longitude
    params = {
        "q": f"{city},{state},US",
        "limit": 1,
        "appid": OPEN_WEATHER_API_KEY,
    }
    url = "http://api.openweathermap.org/geo/1.0/direct"
    r = requests.get(url, params=params)

    api_response_content = json.loads(r.content)

    try:
        latitude = api_response_content[0]["lat"]
        longitude = api_response_content[0]["lon"]
    except (KeyError, IndexError):
        return None

    # Get the current weather for the latitude and longitude found in the previous step
    params = {
        "lat": latitude,
        "lon": longitude,
        "appid": OPEN_WEATHER_API_KEY,
        "units": "imperial",
    }

    url = "https://api.openweathermap.org/data/2.5/weather"
    r = requests.get(url, params=params)

    api_response_content = json.loads(r.content)

    try:
        return {
            "temp": api_response_content["main"]["temp"],
            "description": api_response_content["weather"][0]["description"],
        }
    except (KeyError, IndexError):
        return None
